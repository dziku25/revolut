package service

import domain.user.UserDTO
import model.User
import repository.UsersRepository
import spock.lang.Specification

@SuppressWarnings("GroovyAssignabilityCheck")
class UsersServiceImplTest extends Specification {

    def usersRepository = Mock(UsersRepository)
    def classUnderTest
    def testLogin = "testLogin"
    def testUser

    void setup() {
        classUnderTest = new UsersServiceImpl()
        classUnderTest.setRepository(usersRepository)

        testUser = User.builder().login(testLogin).build()
    }

    def "should return newly created user without accounts when createUser method was called"() {
        given:
        def testUserDTO = UserDTO.builder().login(testLogin).build()

        when:
        def result = classUnderTest.createUser(testUserDTO)

        then:
        1 * usersRepository.addNewUser(testLogin) >> testUser
        1 * usersRepository.findByLogin(testLogin) >> null

        and:
        result.login == testUserDTO.login
        !result.accounts

        0 * _._
    }

}
