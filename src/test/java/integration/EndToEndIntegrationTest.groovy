package integration

import api.App
import converter.JsonConverter
import currency.Currency
import domain.ErrorMessage
import domain.account.AccountDTO
import domain.account.AccountsSummary
import domain.transfer.PaymentDTO
import domain.transfer.TransferDTO
import domain.user.UserDTO
import exception.ErrorMessages
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.HttpResponseException
import spock.lang.Unroll

import static groovyx.net.http.Method.POST
import static groovyx.net.http.ContentType.*
import spock.lang.Specification
import spock.lang.Stepwise
import static groovy.json.JsonOutput.*

import static spark.Spark.stop

@Stepwise
class EndToEndIntegrationTest extends Specification{

    def httpClient
    def result

    def setup() {
        httpClient = new HTTPBuilder('http://localhost:4567')

        App.main(null);
    }

    def teardown() {
        stop()
    }

    def "should be no users in database at startup"() {
        when:
        result = httpClient.get( path: '/users')

        then:
        result == []
    }

    @Unroll
    def "should create user: #name"() {
        given:

        result = httpClient.post(path: '/users', body: [login:name], requestContentType: JSON)
        expect:
        toJson(result) == JsonConverter.serialize(UserDTO.builder().login(name).build())

        where:
        name << ["user1", "user2"]
    }

    def "should create account for user1 in GBP"() {
        when:
        result = httpClient.post(path: '/accounts', body: [owner:"user1", currency:"GBP"], requestContentType: JSON)

        then:
        def deserialize = JsonConverter.deserialize(toJson(result), AccountDTO.class)

        and:
        deserialize.currency == Currency.GBP
        !deserialize.balance
        deserialize.owner == 'user1'
        deserialize.number == '1000001'
    }

    def "should create account for user2 in EURO"() {
        when:
        result = httpClient.post(path: '/accounts', body: [owner:"user2", currency:"EURO"], requestContentType: JSON)

        then:
        def deserialize = JsonConverter.deserialize(toJson(result), AccountDTO.class)

        and:
        deserialize.currency == Currency.EURO
        !deserialize.balance
        deserialize.owner == 'user2'
        deserialize.number == '1000002'
    }

    def "should be able to make 700EURO external payment to user1 account even though the account currency is GBP"() {
        when:
        result = httpClient.post(path: '/payments', body: [accountNumber:"1000001", currency:"EURO", amount:"700"], requestContentType: JSON)

        then:
        def deserialize = JsonConverter.deserialize(toJson(result), PaymentDTO.class)

        and:
        deserialize.accountNumber == '1000001'
        deserialize.amount == BigDecimal.valueOf(700)
    }

    def "should get user1 account with updated balance in correct currency"() {
        when:
        result = httpClient.get( path: '/accounts/user1')

        then:
        def deserialize = JsonConverter.deserialize(toJson(result), AccountsSummary.class)

        and:
        deserialize.accounts.size() == 1
        deserialize.accounts.get(0).owner == 'user1'
        deserialize.accounts.get(0).balance == BigDecimal.valueOf(632.91)
        deserialize.accounts.get(0).currency == Currency.GBP
    }

    def "should be able to transfer 100GBP from user1 to user2 even though accounts are in different currencies"() {
        when:
        result = httpClient.post(path: '/transfers', body: [sourceAccountNumber:"1000001", destinationAccountNumber:"1000002", amount:"100"], requestContentType: JSON)

        then:
        def deserialize = JsonConverter.deserialize(toJson(result), TransferDTO.class)

        and:
        deserialize.sourceAccountNumber == '1000001'
        deserialize.destinationAccountNumber == '1000002'
        deserialize.amount == BigDecimal.valueOf(100)
    }

    def "should get user2 account with updated balance in correct currency"() {
        when:
        result = httpClient.get( path: '/accounts/user2')

        then:
        def deserialize = JsonConverter.deserialize(toJson(result), AccountsSummary.class)

        and:
        deserialize.accounts.size() == 1
        deserialize.accounts.get(0).owner == 'user2'
        deserialize.accounts.get(0).balance == BigDecimal.valueOf(110.6)
        deserialize.accounts.get(0).currency == Currency.EURO
    }

    def "should get user1 account and see that the balance after transfer is decreased by 100GBP"() {
        when:
        result = httpClient.get( path: '/accounts/user1')

        then:
        def deserialize = JsonConverter.deserialize(toJson(result), AccountsSummary.class)

        and:
        deserialize.accounts.size() == 1
        deserialize.accounts.get(0).owner == 'user1'
        deserialize.accounts.get(0).balance == BigDecimal.valueOf(532.91)
        deserialize.accounts.get(0).currency == Currency.GBP
    }

    def "should not be able to create account for not existing user"() {
        when:
        result = httpClient.post(path: '/accounts', body: [owner:"notExistingTom", currency:"EURO"], requestContentType: JSON)

        then:
        thrown(HttpResponseException)
    }

    def "should not be able to create payment with negative amount of money"() {
        when:
        result = httpClient.post(path: '/payments', body: [accountNumber:"1000001", currency:"EURO", amount:"-1"], requestContentType: JSON)

        then:
        thrown(HttpResponseException)
    }

    def "should not be able to create payment with zero amount of money"() {
        when:
        result = httpClient.post(path: '/payments', body: [accountNumber:"1000001", currency:"EURO", amount:"0"], requestContentType: JSON)

        then:
        thrown(HttpResponseException)
    }

    def "should not be able to create payment for non existing account"() {
        when:
        result = httpClient.post(path: '/payments', body: [accountNumber:"1000004", currency:"EURO", amount:"800"], requestContentType: JSON)

        then:
        thrown(HttpResponseException)
    }
}
