package constants;


public class ControllerConsts {

    public static final String DEFAULT_RESPONSE_CONTENT_TYPE = "application/json";
    public static final int HTTP_417 = 417;
    public static final int HTTP_404 = 404;
    public static final int HTTP_400 = 400;

}
