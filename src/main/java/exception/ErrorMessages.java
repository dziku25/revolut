package exception;

public class ErrorMessages {

    public static final String ACCOUNT_CREATION_ERROR_OWNER_NOT_EXISTS = "Cannot create new account for user:%s, user not exists";
    public static final String PAYMENT_ERROR_ACCOUNT_NOT_EXISTS = "Cannot process payment to account:%s, account not exists";
    public static final String USERS_ERROR_LOGIN_EXISTS = "User with login:%s already eixists";
    public static final String USER_NOT_FOUND_ERROR = "Cannot find user with login:%s";
    public static final String ACCOUNT_NOT_FOUND_ERROR = "Cannot find account with number:%s";
    public static final String TRANSFER_ERROR_NOT_ENOUGH_BALANCE = "Cannot process transfer from account:%s, insufficient balance";
    public static final String REQUEST_NOT_VALID_NEGATIVE_AMOUNT = "Request cannot be processed. The amount value should be positive.";
}
