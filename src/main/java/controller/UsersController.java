package controller;

import converter.JsonConverter;
import domain.user.UserDTO;
import service.UsersService;
import service.UsersServiceImpl;

import static constants.ControllerConsts.DEFAULT_RESPONSE_CONTENT_TYPE;
import static converter.JsonConverter.toJson;
import static spark.Spark.post;
import static spark.Spark.get;

public class UsersController extends AbstractController{

    private UsersService service = new UsersServiceImpl();

    private static final String USERS_ENDPOINT = "/users";
    private static final String LOGIN_REQUEST_PARAM = ":login";

    @Override
    void createEndpoints() {
        createNewUserEndpoint();
        createGetUserByLoginEndpoint();
        createGetAllUsersEndpoint();
    }

    private void createGetAllUsersEndpoint() {
        get(USERS_ENDPOINT, (req, res) -> {
            res.type(DEFAULT_RESPONSE_CONTENT_TYPE);
            return service.getAllUsers();
        }, toJson());
    }

    private void createGetUserByLoginEndpoint() {
        get(USERS_ENDPOINT + "/" + LOGIN_REQUEST_PARAM, (req, res) -> {
            res.type(DEFAULT_RESPONSE_CONTENT_TYPE);
            return service.getUserByLogin(req.params(LOGIN_REQUEST_PARAM));
        }, toJson());
    }

    private void createNewUserEndpoint() {
        post(USERS_ENDPOINT, (req, res) -> {
            res.type(DEFAULT_RESPONSE_CONTENT_TYPE);
            return service.createUser(JsonConverter.deserialize(req.body(), UserDTO.class));
        }, toJson());
    }

}
