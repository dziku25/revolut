package controller;


import converter.JsonConverter;
import domain.transfer.PaymentDTO;
import domain.transfer.TransferDTO;
import exception.ValidationException;
import service.TransfersService;
import service.TransfersServiceImpl;
import validation.PaymentValidator;
import validation.RequestValidator;
import validation.TransferValidator;

import static constants.ControllerConsts.DEFAULT_RESPONSE_CONTENT_TYPE;
import static converter.JsonConverter.toJson;
import static exception.ErrorMessages.REQUEST_NOT_VALID_NEGATIVE_AMOUNT;
import static spark.Spark.post;

public class TransfersController extends AbstractController{

    private static final String PAYMENTS_ENDPOINT = "/payments";
    private static final String TRANSFERS_ENDPOINT = "/transfers";

    private TransfersService service = new TransfersServiceImpl();
    private RequestValidator<PaymentDTO> paymentValidator = new PaymentValidator();
    private RequestValidator<TransferDTO> transferValidator = new TransferValidator();

    @Override
    void createEndpoints() {
        createTransfersEndpoint();
        createNewPaymentEndpoint();
    }

    private void createNewPaymentEndpoint() {
        post(PAYMENTS_ENDPOINT, (request, response) -> {
            PaymentDTO requestBody = JsonConverter.deserialize(request.body(), PaymentDTO.class);

            if(!paymentValidator.isValid(requestBody))
                throw new ValidationException(REQUEST_NOT_VALID_NEGATIVE_AMOUNT);

            response.type(DEFAULT_RESPONSE_CONTENT_TYPE);
            return service.createPayment(requestBody);
        }, toJson());
    }

    private void createTransfersEndpoint() {
        post(TRANSFERS_ENDPOINT, (request, response) -> {
            TransferDTO requestBody = JsonConverter.deserialize(request.body(), TransferDTO.class);

            if(!transferValidator.isValid(requestBody))
                throw new ValidationException(REQUEST_NOT_VALID_NEGATIVE_AMOUNT);

            response.type(DEFAULT_RESPONSE_CONTENT_TYPE);
            return service.createTransfer(requestBody);
        }, toJson());
    }
}
