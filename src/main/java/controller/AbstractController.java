package controller;

public abstract class AbstractController {

    public AbstractController() {
        createEndpoints();
    }

    abstract void createEndpoints();
}
