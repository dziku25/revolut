package controller;

import static constants.ControllerConsts.DEFAULT_RESPONSE_CONTENT_TYPE;
import static converter.JsonConverter.toJson;
import static spark.Spark.get;
import static spark.Spark.post;

import converter.JsonConverter;
import domain.account.AccountDTO;
import service.AccountsService;
import service.AccountsServiceImpl;

public class AccountsController extends AbstractController {

    private AccountsService service = new AccountsServiceImpl();
    private static final String ACCOUNTS_ENDPOINT = "/accounts";
    private static final String OWNER_REQUEST_PARAM = ":owner";

    @Override
    void createEndpoints() {
        createAllAccountsEndpoint();
        createNewAccountEndpoint();
        createAccountsByOwnerEndpoint();
    }

    private void createAccountsByOwnerEndpoint() {
        get(ACCOUNTS_ENDPOINT + "/" + OWNER_REQUEST_PARAM, (request, response) -> {
            response.type(DEFAULT_RESPONSE_CONTENT_TYPE);
            return service.getUserAccounts(request.params(OWNER_REQUEST_PARAM));
        }, toJson());
    }

    private void createNewAccountEndpoint() {
        post(ACCOUNTS_ENDPOINT, (request, response) -> {
            response.type(DEFAULT_RESPONSE_CONTENT_TYPE);
            return service.createNewAccount(JsonConverter.deserialize(request.body(), AccountDTO.class));
        }, toJson());
    }

    private void createAllAccountsEndpoint() {
        get(ACCOUNTS_ENDPOINT, (request, response) -> {
            response.type(DEFAULT_RESPONSE_CONTENT_TYPE);
            return service.getAllAccounts();
        }, toJson());
    }

}
