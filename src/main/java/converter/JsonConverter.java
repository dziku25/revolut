package converter;


import com.google.gson.Gson;
import exception.ProcessingException;
import spark.ResponseTransformer;

public class JsonConverter {

    public static String serialize(Object object) {
        return new Gson().toJson(object);
    }

    public static <T> T deserialize(String json, Class<T> clazz) {
        try{
            return new Gson().fromJson(json, clazz);
        } catch (Exception e) {
            throw new ProcessingException(e.getMessage());
        }
    }

    public static ResponseTransformer toJson() {
        return JsonConverter::serialize;
    }

}
