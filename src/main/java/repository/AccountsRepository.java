package repository;

import currency.Currency;
import model.Account;
import model.User;

import java.util.List;

public interface AccountsRepository {
    List<Account> getAllAccounts();

    Account createNewAccount(User owner, Currency currency);

    Account findByNumber(String number);

    List<Account> getByOwner(Integer userId);

}
