package repository;

import model.User;

import javax.persistence.NoResultException;
import java.util.List;

public interface UsersRepository {

    User addNewUser(String login);

    List<User> getAllUsers();

    User findByLogin(String login) throws NoResultException;
}
