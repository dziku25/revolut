package repository;


import config.Database;
import exception.ProcessingException;
import model.Account;
import model.Payment;
import model.Transfer;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.EntityTransaction;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

import static exception.ErrorMessages.PAYMENT_ERROR_ACCOUNT_NOT_EXISTS;

public class TransfersRepositoryImpl implements TransfersRepository {

    private AccountsRepository accountsRepository = new AccountsRepositoryImpl();

    @Override
    public Payment createPayment(BigDecimal amount, Account account) {
        Transaction trn = null;
        try (Session session = Database.getSessionFactory().openSession()){
            trn = session.beginTransaction();
            account.setBalance(account.getBalance().add(amount));
            session.update(account);
            Payment payment = Payment.builder().paymentDate(new Date()).amount(amount).account(account).build();
            session.persist(payment);
            trn.commit();
            return payment;
        } catch (Exception e) {
            if (trn != null)
                trn.rollback();
            throw new ProcessingException(e.getMessage());
        }
    }

    @Override
    public Transfer createTransfer(BigDecimal amountInTargetAccountCurrency, BigDecimal amountInSourceAccountCurrency, Account source, Account destination) {
        Transaction trn = null;
        try (Session session = Database.getSessionFactory().openSession()){
            trn = session.beginTransaction();
            destination.setBalance(destination.getBalance().add(amountInTargetAccountCurrency));
            session.update(destination);

            source.setBalance(source.getBalance().subtract(amountInSourceAccountCurrency));
            session.update(source);

            Transfer transfer = Transfer.builder().amount(amountInTargetAccountCurrency).paymentDate(new Date()).source(source).destination(destination).build();
            session.persist(transfer);
            trn.commit();
            return transfer;
        } catch (Exception e) {
            if (trn != null)
                trn.rollback();
            throw new ProcessingException(e.getMessage());
        }
    }
}
