package repository;


import config.Database;
import exception.ProcessingException;
import model.User;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

public class UsersRepositoryImpl implements UsersRepository {

    @Override
    public User addNewUser(String login) {
        Transaction trn = null;
        try (Session session = Database.getSessionFactory().openSession()){
            trn = session.beginTransaction();
            User user = User.builder().login(login).build();
            session.persist(user);
            trn.commit();
            return user;
        } catch (Exception e) {
            Optional.ofNullable(trn).ifPresent(EntityTransaction::rollback);
            throw new ProcessingException(e.getMessage());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<User> getAllUsers() {
        Session session = Database.getSessionFactory().openSession();
        List<User> users = (List<User>)session.createQuery("from User").list();
        session.close();
        return users;
    }

    @SuppressWarnings("unchecked")
    @Override
    public User findByLogin(String login) {
        Session session = Database.getSessionFactory().openSession();
        Query query = session.createQuery("from User where login = :login ");
        query.setParameter("login", login);
        List<User> users = (List<User>)query.getResultList();
        session.close();
        return users.stream().findFirst().orElse(null);
    }
}

