package repository;


import config.Database;
import currency.Currency;
import domain.ErrorMessage;
import exception.ErrorMessages;
import exception.ProcessingException;
import model.Account;
import model.User;
import org.hibernate.Session;
import org.hibernate.Transaction;
import providers.AccountNumberProvider;
import providers.AccountNumberProviderImpl;

import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

public class AccountsRepositoryImpl implements AccountsRepository {

    private AccountNumberProvider accountNumberProvider = new AccountNumberProviderImpl();

    @Override
    public List<Account> getAllAccounts() {
        Session session = Database.getSessionFactory().openSession();
        List<Account> accounts = session.createQuery("from Account", Account.class).list();
        session.close();
        return accounts;
    }

    @Override
    public Account createNewAccount(User owner, Currency currency) {
        Transaction trn = null;
        try (Session session = Database.getSessionFactory().openSession()){
            trn = session.getTransaction();
            trn.begin();
            Account newAccount = Account.builder()
                    .balance(BigDecimal.ZERO)
                    .user(owner)
                    .number(accountNumberProvider.generate())
                    .currency(currency.getName())
                    .build();

            session.persist(newAccount);
            trn.commit();
            return newAccount;
        } catch (Exception e) {
            if (trn != null)
                trn.rollback();

            throw new ProcessingException(e.getMessage());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Account findByNumber(String number) {
        Session session = Database.getSessionFactory().openSession();
        Query query = session.createQuery("from Account where number = :number ");
        query.setParameter("number", number);
        List<Account> account = (List<Account>)query.getResultList();
        session.close();
        return account.stream().findFirst().orElse(null);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Account> getByOwner(Integer userId) {
        Session session = Database.getSessionFactory().openSession();
        Query query = session.createQuery("from Account where user.id = :userId ");
        query.setParameter("userId", userId);
        List<Account> userAccounts = (List<Account>)query.getResultList();
        session.close();
        return userAccounts;
    }

}
