package repository;


import model.Account;
import model.Payment;
import model.Transfer;

import java.math.BigDecimal;

public interface TransfersRepository {

    Payment createPayment(BigDecimal amount, Account account);

    Transfer createTransfer(BigDecimal amountInTargetAccountCurrency,BigDecimal amountInSourceAccountCurrency, Account source, Account destination);
}
