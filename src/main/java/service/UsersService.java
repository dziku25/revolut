package service;


import domain.user.UserDTO;

import java.util.List;

public interface UsersService {

    UserDTO createUser(UserDTO newUser);

    UserDTO getUserByLogin(String login);

    List<UserDTO> getAllUsers();

}
