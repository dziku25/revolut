package service;


import java.math.BigDecimal;

public interface CurrencyService {

    BigDecimal euroToGBP(BigDecimal amount);

    BigDecimal gbpToEuro(BigDecimal amount);
}
