package service;

import currency.Currency;
import domain.transfer.PaymentDTO;
import domain.transfer.TransferDTO;
import exception.ProcessingException;
import exception.ResourceMissingException;
import model.Account;
import model.Payment;
import model.Transfer;
import repository.AccountsRepository;
import repository.AccountsRepositoryImpl;
import repository.TransfersRepository;
import repository.TransfersRepositoryImpl;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.function.Predicate;

import static exception.ErrorMessages.ACCOUNT_NOT_FOUND_ERROR;
import static exception.ErrorMessages.TRANSFER_ERROR_NOT_ENOUGH_BALANCE;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class TransfersServiceImpl implements TransfersService {

    private TransfersRepository transfersRepository = new TransfersRepositoryImpl();
    private AccountsRepository accountsRepository = new AccountsRepositoryImpl();

    private CurrencyService currencyService = new CurrencyServiceImpl();

    @Override
    public PaymentDTO createPayment(PaymentDTO paymentInfo) {
        Account account = accountsRepository.findByNumber(paymentInfo.getAccountNumber());
        if(isNull(account))
            throw new ResourceMissingException(String.format(ACCOUNT_NOT_FOUND_ERROR, paymentInfo.getAccountNumber()));

        BigDecimal amount = paymentInfo.getAmount();
        if (nonNull(paymentInfo.getCurrency()))
           amount = getAmountInTargetAccountCurrency(paymentInfo.getCurrency(), account.getCurrency(), paymentInfo.getAmount());

        Payment payment = transfersRepository.createPayment(amount, account);

        return PaymentDTO.builder()
                .amount(paymentInfo.getAmount())
                .accountNumber(paymentInfo.getAccountNumber())
                .date(payment.getPaymentDate())
                .build();
    }

    private BigDecimal getAmountInTargetAccountCurrency(Currency transferCurrency, String targetCurrency, BigDecimal amount) {
        if(transferCurrency.equals(Currency.valueOf(targetCurrency)))
            return amount;

        return transferCurrency.equals(Currency.EURO) ? currencyService.euroToGBP(amount) : currencyService.gbpToEuro(amount);
    }

    @Override
    public TransferDTO createTransfer(TransferDTO transferInfo) {
        Account source = accountsRepository.findByNumber(transferInfo.getSourceAccountNumber());
        if(isNull(source))
            throw new ResourceMissingException(String.format(ACCOUNT_NOT_FOUND_ERROR, transferInfo.getSourceAccountNumber()));

        Account destination = accountsRepository.findByNumber(transferInfo.getDestinationAccountNumber());
        if(isNull(destination))
            throw new ResourceMissingException(String.format(ACCOUNT_NOT_FOUND_ERROR, transferInfo.getDestinationAccountNumber()));

        BigDecimal amountInSourceAccountCurrency = transferInfo.getAmount();
        BigDecimal amountInTargetAccountCurrency = getAmountInTargetAccountCurrency(Currency.valueOf(source.getCurrency()), destination.getCurrency(), transferInfo.getAmount());

        if(!checkIfSourceAccountHasEnoughBalanceForTransfer(source, amountInSourceAccountCurrency))
            throw new ProcessingException(String.format(TRANSFER_ERROR_NOT_ENOUGH_BALANCE, source.getNumber()));

        Transfer transfer = transfersRepository.createTransfer(amountInTargetAccountCurrency, amountInSourceAccountCurrency, source, destination);
        return mapToTransferDTO(transferInfo, transfer);
    }

    private boolean checkIfSourceAccountHasEnoughBalanceForTransfer(Account source, BigDecimal amount) {
        return source.getBalance().compareTo(amount) >= 0;
    }

    private TransferDTO mapToTransferDTO(TransferDTO transferInfo, Transfer transfer) {
        return TransferDTO.builder()
                .amount(transferInfo.getAmount())
                .date(transfer.getPaymentDate())
                .destinationAccountNumber(transferInfo.getDestinationAccountNumber())
                .sourceAccountNumber(transferInfo.getSourceAccountNumber())
                .build();
    }

}
