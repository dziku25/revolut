package service;

import domain.user.UserDTO;
import exception.ProcessingException;
import exception.ResourceMissingException;
import model.Account;
import model.User;
import repository.UsersRepository;
import repository.UsersRepositoryImpl;

import javax.persistence.NoResultException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static exception.ErrorMessages.USERS_ERROR_LOGIN_EXISTS;
import static exception.ErrorMessages.USER_NOT_FOUND_ERROR;

public class UsersServiceImpl implements UsersService{

    private UsersRepository repository = new UsersRepositoryImpl();

    void setRepository(UsersRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserDTO createUser(UserDTO newUser) {
        User existingUser = repository.findByLogin(newUser.getLogin());
        if(Optional.ofNullable(existingUser).isPresent())
            throw new ProcessingException(String.format(USERS_ERROR_LOGIN_EXISTS, newUser.getLogin()));

        User user = repository.addNewUser(newUser.getLogin());
        return UserDTO.builder().login(user.getLogin()).build();
    }

    @Override
    public UserDTO getUserByLogin(String login) {
        User user = repository.findByLogin(login);

        return Optional.ofNullable(user).map(user1 -> UserDTO.builder().login(user1.getLogin()).build())
                .orElseThrow(() -> new ResourceMissingException(String.format(USER_NOT_FOUND_ERROR, login)));
    }

    @Override
    public List<UserDTO> getAllUsers() {
        List<User> users = repository.getAllUsers();
        return users.stream()
                .map(user -> UserDTO.builder().login(user.getLogin()).build())
                .collect(Collectors.toList());
    }
}
