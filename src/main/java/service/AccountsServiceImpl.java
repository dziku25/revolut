package service;

import currency.Currency;
import domain.account.AccountDTO;
import domain.account.AccountsSummary;
import domain.user.UserDTO;
import exception.ErrorMessages;
import exception.ProcessingException;
import exception.ResourceMissingException;
import model.Account;
import model.User;
import repository.AccountsRepository;
import repository.AccountsRepositoryImpl;
import repository.UsersRepository;
import repository.UsersRepositoryImpl;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

public class AccountsServiceImpl implements AccountsService {

    private AccountsRepository accountsRepository;
    private UsersRepository usersRepository;

    public AccountsServiceImpl() {
        accountsRepository = new AccountsRepositoryImpl();
        usersRepository = new UsersRepositoryImpl();
    }

    @Override
    public AccountsSummary getAllAccounts() {
        List<Account> accounts = accountsRepository.getAllAccounts();
        return mapToAccountsSummary(accounts);
    }

    private AccountsSummary mapToAccountsSummary(List<Account> accounts) {
        return AccountsSummary.builder()
                .accounts(mapToDTO(accounts))
                .build();
    }

    @Override
    public AccountDTO createNewAccount(AccountDTO accountDTO) {
        User owner = usersRepository.findByLogin(accountDTO.getOwner());
        if(isNull(owner))
            throw new ResourceMissingException(String.format(ErrorMessages.ACCOUNT_CREATION_ERROR_OWNER_NOT_EXISTS, accountDTO.getOwner()));

        Account newAccount = accountsRepository.createNewAccount(owner, accountDTO.getCurrency());
        return AccountDTO.builder()
                .number(newAccount.getNumber())
                .owner(newAccount.getUser().getLogin())
                .currency(Currency.valueOf(newAccount.getCurrency()))
                .build();
    }

    @Override
    public AccountsSummary getUserAccounts(String ownerLogin) {
        User owner = usersRepository.findByLogin(ownerLogin);
        if(isNull(owner))
            throw new ResourceMissingException(String.format(ErrorMessages.USER_NOT_FOUND_ERROR, ownerLogin));

        List<Account> userAccounts = accountsRepository.getByOwner(owner.getId());
        return mapToAccountsSummary(userAccounts);
    }

    private List<AccountDTO> mapToDTO(List<Account> accounts) {
        return accounts.stream()
                .map(account -> AccountDTO.builder()
                        .number(account.getNumber())
                        .owner(account.getUser().getLogin())
                        .balance(account.getBalance())
                        .currency(Currency.valueOf(account.getCurrency()))
                        .build())
                .collect(Collectors.toList());
    }
}
