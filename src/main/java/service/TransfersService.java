package service;


import domain.transfer.PaymentDTO;
import domain.transfer.TransferDTO;

public interface TransfersService {

    PaymentDTO createPayment(PaymentDTO paymentInfo);

    TransferDTO createTransfer(TransferDTO transferInfo);

}
