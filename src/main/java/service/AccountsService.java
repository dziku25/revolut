package service;


import domain.account.AccountDTO;
import domain.account.AccountsSummary;

public interface AccountsService {
    AccountsSummary getAllAccounts();
    AccountDTO createNewAccount(AccountDTO newAccount);
    AccountsSummary getUserAccounts(String login);
}
