package service;

import currency.Currency;
import currency.CurrencyMultiplierProvider;
import currency.DefaultCurrencyMultiplierProvider;

import java.math.BigDecimal;

import static java.math.BigDecimal.ROUND_HALF_UP;

public class CurrencyServiceImpl implements CurrencyService{

    private CurrencyMultiplierProvider currencyMultiplierProvider = new DefaultCurrencyMultiplierProvider();

    private static final int DEFAULT_ROUNDING_SCALE = 2;
    private static final int DEFAULT_ROUNDING_MODE = ROUND_HALF_UP;

    @Override
    public BigDecimal euroToGBP(BigDecimal amount) {
        BigDecimal gbp = amount.multiply(currencyMultiplierProvider.getEuroToGBP());
        return gbp.setScale(DEFAULT_ROUNDING_SCALE, DEFAULT_ROUNDING_MODE);
    }

    @Override
    public BigDecimal gbpToEuro(BigDecimal amount) {
        BigDecimal gbp = amount.multiply(currencyMultiplierProvider.getGBPToEuro());
        return gbp.setScale(DEFAULT_ROUNDING_SCALE, DEFAULT_ROUNDING_MODE);
    }

}
