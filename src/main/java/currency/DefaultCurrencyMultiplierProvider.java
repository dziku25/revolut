package currency;


import java.math.BigDecimal;

public class DefaultCurrencyMultiplierProvider implements CurrencyMultiplierProvider {

    private static final String EURO_TO_GBP = "0.90415121";
    private static final String GBP_TO_EURO = "1.10600969";

    @Override
    public BigDecimal getGBPToEuro() {
        return new BigDecimal(GBP_TO_EURO);
    }

    @Override
    public BigDecimal getEuroToGBP() {
        return new BigDecimal(EURO_TO_GBP);
    }
}
