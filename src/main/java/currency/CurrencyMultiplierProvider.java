package currency;

import java.math.BigDecimal;

public interface CurrencyMultiplierProvider {

    BigDecimal getGBPToEuro();

    BigDecimal getEuroToGBP();
}
