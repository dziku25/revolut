package currency;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Currency {

    EURO("EURO"),
    GBP("GBP");

    @Getter
    private final String name;
}
