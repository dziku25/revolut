package api;

import controller.AccountsController;
import controller.TransfersController;
import controller.UsersController;
import converter.JsonConverter;
import domain.ErrorMessage;
import exception.ProcessingException;
import exception.ResourceMissingException;
import exception.ValidationException;
import spark.Response;

import static constants.ControllerConsts.HTTP_400;
import static constants.ControllerConsts.HTTP_404;
import static constants.ControllerConsts.HTTP_417;
import static spark.Spark.exception;

public class App {

    public static void main( String[] args) {
        init();
    }

    private static void init() {
        createControllers();
        registerExceptionHandlers();
    }

    private static void registerExceptionHandlers() {
        exception(ProcessingException.class, (exception, request, response) -> {
            prepareErrorResponse(response, HTTP_417, JsonConverter.serialize(ErrorMessage.builder()
                    .error(exception.getMessage())
                    .build()));
        });
        exception(ResourceMissingException.class, (exception, request, response) -> {
            prepareErrorResponse(response, HTTP_404, JsonConverter.serialize(ErrorMessage.builder()
                    .error(exception.getMessage())
                    .build()));
        });
        exception(ValidationException.class, (exception, request, response) -> {
            prepareErrorResponse(response, HTTP_400, JsonConverter.serialize(ErrorMessage.builder()
                    .error(exception.getMessage())
                    .build()));
        });
    }

    private static void prepareErrorResponse(Response response, int statusCode, String body) {
        response.status(statusCode);
        response.body(body);
    }

    private static void createControllers() {
        new AccountsController();
        new UsersController();
        new TransfersController();
    }
}
