package config;

import model.Account;
import model.Payment;
import model.Transfer;
import model.User;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import java.util.Arrays;
import java.util.List;

import static java.util.Objects.isNull;

public class Database {

    private static List<Class> annotatedClasses = Arrays.asList(Account.class, User.class, Payment.class, Transfer.class);
    private static SessionFactory sessionFactory;

    private static SessionFactory createSessionFactory() {
        Configuration configuration = new Configuration().configure();
        annotatedClasses.forEach(configuration::addAnnotatedClass);
        StandardServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties())
                .build();
        return configuration.buildSessionFactory(serviceRegistry);
    }

    public static SessionFactory getSessionFactory() {
        if (isNull(sessionFactory)) {
            synchronized (Database.class) {
                if (isNull(sessionFactory))
                    sessionFactory= createSessionFactory();
            }
        }
        return sessionFactory;
    }
}
