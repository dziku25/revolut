package providers;


public interface AccountNumberProvider {

    String generate();
}
