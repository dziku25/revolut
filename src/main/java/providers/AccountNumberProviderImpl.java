package providers;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class AccountNumberProviderImpl implements AccountNumberProvider{

    private AtomicLong number = new AtomicLong(1000000L);

    @Override
    public String generate() {
        return String.valueOf(number.incrementAndGet());
    }
}
