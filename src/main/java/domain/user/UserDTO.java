package domain.user;

import domain.account.AccountDTO;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UserDTO {

    private String login;
    private List<String> accounts;

}
