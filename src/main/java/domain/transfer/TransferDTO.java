package domain.transfer;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;

@Data
@Builder
public class TransferDTO {
    private Date date;
    private BigDecimal amount;
    private String sourceAccountNumber;
    private String destinationAccountNumber;
}
