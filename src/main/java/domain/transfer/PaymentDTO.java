package domain.transfer;


import currency.Currency;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
public class PaymentDTO {
    private Date date;
    private BigDecimal amount;
    private String accountNumber;
    private Currency currency;
}
