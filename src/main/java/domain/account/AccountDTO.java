package domain.account;

import currency.Currency;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class AccountDTO {
    private String number;
    private String owner;
    private BigDecimal balance;
    private Currency currency;
}
