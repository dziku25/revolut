package domain.account;

import domain.account.AccountDTO;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class AccountsSummary {
    private List<AccountDTO> accounts;
}
