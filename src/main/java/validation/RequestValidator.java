package validation;

public interface RequestValidator<T> {

    boolean isValid (T request);
}
