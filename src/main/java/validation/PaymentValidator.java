package validation;


import domain.transfer.PaymentDTO;

public class PaymentValidator implements RequestValidator<PaymentDTO>, AmountValidator {

    @Override
    public boolean isValid(PaymentDTO request) {
        return isAcceptable(request.getAmount());
    }
}
