package validation;

import domain.transfer.TransferDTO;

public class TransferValidator implements RequestValidator<TransferDTO>, AmountValidator{

    @Override
    public boolean isValid(TransferDTO request) {
        return isAcceptable(request.getAmount());
    }
}
