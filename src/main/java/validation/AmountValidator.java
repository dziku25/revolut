package validation;

import java.math.BigDecimal;

public interface AmountValidator {

    default boolean isAcceptable(BigDecimal amount) {
        return amount.compareTo(BigDecimal.ZERO) > 0;
    }

}
