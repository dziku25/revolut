package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.util.List;

import static model.User.TABLE_NAME;

@Entity
@Table(name = TABLE_NAME)
@AllArgsConstructor
@Data
@Builder
@NoArgsConstructor
public class User {

    static final String TABLE_NAME = "USER";
    private static final String COLUMN_ID = "USER_ID";
    private static final String COLUMN_LOGIN = "LOGIN";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = COLUMN_ID)
    private Integer id;

    @Column(name = COLUMN_LOGIN)
    private String login;

    @OneToMany(mappedBy = "user")
    private List<Account> accounts;
}
