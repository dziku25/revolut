package model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

import static model.Transfer.TABLE_NAME;


@Entity
@Table(name = TABLE_NAME)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Transfer {

    static final String TABLE_NAME = "TRANSFERS";
    private static final String COLUMN_ID = "TRANSFER_ID";
    private static final String COLUMN_PAYMENT_DATE = "TRANSFER_DATE";
    private static final String COLUMN_AMOUNT = "AMOUNT";
    private static final String COLUMN_SOURCE_ACCOUNT = "SOURCE_ACCOUNT_ID";
    private static final String COLUMN_DEST_ACCOUNT = "DEST_ACCOUNT_ID";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = COLUMN_ID)
    private Integer id;

    @Column(name = COLUMN_AMOUNT)
    private BigDecimal amount;

    @Column(name = COLUMN_PAYMENT_DATE)
    private Date paymentDate;

    @ManyToOne
    private Account source;

    @ManyToOne
    private Account destination;
}
