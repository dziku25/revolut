package model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import static model.Account.TABLE_NAME;

@Entity
@Table(name = TABLE_NAME)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Account {

    static final String TABLE_NAME = "ACCOUNT";
    private static final String COLUMN_ID = "ACCOUNT_ID";
    private static final String COLUMN_NUMBER = "NUMBER";
    private static final String COLUMN_BALANCE = "BALANCE";
    private static final String COLUMN_CURRENCY = "CURRENCY";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = COLUMN_ID)
    private Integer id;

    @Column(name = COLUMN_NUMBER)
    private String number;

    @Column(name = COLUMN_BALANCE)
    private BigDecimal balance;

    @Column(name = COLUMN_CURRENCY)
    private String currency;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private User user;

    @OneToMany(mappedBy = "account")
    List<Payment> payments;

    @OneToMany(mappedBy = "source")
    List<Transfer> transfersSource;

    @OneToMany(mappedBy = "destination")
    List<Transfer> transfersDestination;
}
