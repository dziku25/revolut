package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

import static model.Payment.TABLE_NAME;


@Entity
@Table(name = TABLE_NAME)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Payment {

    static final String TABLE_NAME = "PAYMENT";
    private static final String COLUMN_ID = "PAYMENT_ID";
    private static final String COLUMN_PAYMENT_DATE = "PAYMENT_DATE";
    private static final String COLUMN_AMOUNT = "AMOUNT";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = COLUMN_ID)
    private Integer id;

    @Column(name = COLUMN_AMOUNT)
    private BigDecimal amount;

    @Column(name = COLUMN_PAYMENT_DATE)
    private Date paymentDate;

    @ManyToOne
    private Account account;
}
