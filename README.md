# Revolut test

This simple REST Api application was created for Revolut interview test. Below you can find a simple API documentation which describes all available endpoints. You can use Postman tool to play a little bit with the API. 

Feel free to run EndToEndIntegrationTest in order to see some example flow of the requests and learn more how to use it. 

Application allows to create users. For each user you can create an account. In this simple version you need to choose from GBP or EURO currency while creating a new account. For newly created account the balance is set to zero. In order to put some money you need to create an external payment using `/payments` endpoint. Then you can feel free to make internal transfers between accounts or make more payments. Conversions between currencies are done one the fly but the account balance is always in the currency chosen on creation.
  
The API could be extended to offer all payments/transfers for given account etc but it was a time-boxed activity so I didn't implemented all features that came to my mind.

#### Technology stack
* JDK 8
* Hibernate
* H2database
* Lombok (http://projectlombok.org/)
* Groovy/Spock for testing (http://spockframework.org/)
* Spark framework (http://sparkjava.com/)

NOTICE: where's Spring ? :):):) 

#API Documentation
**Create new user**
----
  Creates new user. In order to create accounts or make payments you need to create a new user first.

* **URL**

  /users

* **Method:**

  `POST`
  
* **Request body**

    ```
      {
          "login": "john1"
      }
    ```

* **Success Response:**

  * **Code:** 200
    **Content:** `{"login":"john1"}`
 
* **Error Response:**

  * **Code:** 417 Expectation Failed <br />
    **Content:** `{"error":"User with login:john1 already eixists"}`
    
**Show all users**
----
   Shows list of all users. 

* **URL**

  /users

* **Method:**

  `GET`
  
* **Success Response:**

  * **Code:** 200
    **Content:** `[{"login":"admin1"},{"login":"admin2"},{"login":"admin3"}]`

**Show specific user**
----
   Shows user data by provided query param: `login`.  

* **URL**

  /users/:login

* **Method:**

  `GET`
  
* **Query parameters**

    * login

* **Success Response:**

  * **Code:** 200
    **Content:** `{ "login": "john1" }`
 
* **Error Response:**

  * **Code:** 404 Not Found <br />
    **Content:** `{"error":"Cannot find user with login:john1"}`
        
**Create new account**
----
  Creates new account associated with given user.

* **URL**

  /accounts

* **Method:**

  `POST`
  
* **Request body**

    ```
      {
          "owner": "john1"
          "currency":"GBP"
      }
    ```

* **Success Response:**

  * **Code:** 200
    **Content:** `{"number":"e4d9d434-e686-48d2-8bdf-f98c2302310a","owner":"john1","currency":"GBP"}`
 
* **Error Response:**

  * **Code:** 404 Not Found <br />
    **Content:** `{"error":"Cannot create new account for user:john9, user not exists"}` 
        
**Show all accounts**
----
   Shows list of all accounts. 

* **URL**

  /accounts

* **Method:**

  `GET`
  
* **Success Response:**

  * **Code:** 200
    **Content:** `{"accounts":[{"number":"cfe249e4-78e1-4467-bc9c-a63f975bfe03","owner":"admin3","balance":0.00,"currency":"GBP"},{"number":"da0ef149-9d43-497a-8313-3f3cf6a48ef9","owner":"admin2","balance":0.00,"currency":"EURO"}]}`
    
**Show user accounts**
----
   Shows list of all accounts for specified user by `:owner` query param. 

* **URL**

  /accounts/:owner

* **Method:**

  `GET`
  
* **Query parameters**
  
   * owner - value of this param should be equal to user's login
  
* **Success Response:**

  * **Code:** 200
    **Content:** `{"accounts":[{"number":"e5d11679-d751-4c63-ab3e-8ccdfdad2c2b","owner":"john2","balance":0.00,"currency":"GBP"},{"number":"4f6d418a-e632-4729-aead-27fb628c57f1","owner":"john2","balance":0.00,"currency":"EURO"}]}`    
      
* **Error Response:**

  * **Code:** 404 Not Found <br />
    **Content:** `{"error":"Cannot find user with login:john22"}`
    
**Create new payment**
----
  Creates new payment for account.

* **URL**

  /payments

* **Method:**

  `POST`
  
* **Request body**

    ```
      {
      	"accountNumber":"394efba0-5c4f-43f7-9be9-c2027c628245",
      	"amount": 1500
      	"currency":"GBP"
      }
    ```

* **Success Response:**

  * **Code:** 200
    **Content:** `{"date":"Aug 7, 2017 11:01:45 PM","amount":1500,"accountNumber":"394efba0-5c4f-43f7-9be9-c2027c628245"}`
 
* **Error Response:**

  * **Code:** 404 Not Found <br />
    **Content:** `{"error":"Cannot find account with number:notExisting"}` 
      
  * **Code:** 417 Expectation Failed <br />
    **Content:** `Request cannot be processed. The amount value should be positive.`   
      
**Create new transfer**
----
  Creates new money transfer from source to destination account.

* **URL**

  /payments

* **Method:**

  `POST`
  
* **Request body**

    ```
      {
      	"sourceAccountNumber":"394efba0-5c4f-43f7-9be9-c2027c628245",
      	"destinationAccountNumber":"e5d11679-d751-4c63-ab3e-8ccdfdad2c2b",
      	"amount": 1500
      }
    ```

* **Success Response:**

  * **Code:** 200
    **Content:** `{"date":"Aug 7, 2017 11:01:45 PM","amount":1500,"sourceAccountNumber":"394efba0-5c4f-43f7-9be9-c2027c628245","destinationAccountNumber":"e5d11679-d751-4c63-ab3e-8ccdfdad2c2b"}`
 
* **Error Response:**

  * **Code:** 404 Not Found <br />
    **Content:** `{"error":"Cannot find account with number:notExisting"}` 
      
  * **Code:** 417 Expectation Failed <br />
    **Content:** `Request cannot be processed. The amount value should be positive.`       